package br.ifsc.edu.minhasnotinhas;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaNotas extends AppCompatActivity {

    ListView listView;
    SQLiteDatabase bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notas);

        listView = findViewById(R.id.listView);
        bd = openOrCreateDatabase("meubd",MODE_PRIVATE, null);
        Cursor cadastros = bd.query("alunos", null, null, null, null, null, null);
        cadastros.moveToFirst();
        ArrayList<String> itens = new ArrayList<>();
        do{
            String s = new String();
            s = cadastros.getString(cadastros.getColumnIndex("titulo"));
            itens.add(s);
        }   while (cadastros.moveToNext());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                itens);
        listView.setAdapter(adapter);


    }
}
