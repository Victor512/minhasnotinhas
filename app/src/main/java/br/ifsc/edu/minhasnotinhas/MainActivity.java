package br.ifsc.edu.minhasnotinhas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase bd;

    ListView listView;
    EditText editCodigo;
    EditText editTitulo;
    EditText editTexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bd = openOrCreateDatabase("meubd",MODE_PRIVATE, null);
        bd.execSQL("CREATE TABLE IF NOT EXISTS notas" +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "titulo varchar(50) NOT NULL," +
                "texto varchar(1500));");

        editCodigo = findViewById(R.id.editTextCodigo);
        editTitulo = findViewById(R.id.editTextTitulo);
        editTexto = findViewById(R.id.editTextTexto);

        Cursor cursor = bd.rawQuery("SELECT * FROM notas",null,null);
        cursor.moveToFirst();

        String id, titulo, texto;
        final ArrayList<String> arrayList = new ArrayList<>();

        while (!cursor.isAfterLast()){
            id = cursor.getString(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            texto = cursor.getString(cursor.getColumnIndex("texto"));

            arrayList.add(id+" "+titulo+" "+texto);
            cursor.moveToNext();
        }

        listView = findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                arrayList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               Intent intent = new Intent(getApplicationContext(),MainActivity.class);
               startActivity(intent);
            }
        });
    }

    public void Cadastrar(View view) {
        ContentValues cadastro = new ContentValues();
        cadastro.put("titulo",editTitulo.getText().toString());
        cadastro.put("texto",editTexto.getText().toString());
        bd.insert("notas", cadastro, null, null);
        String s = getResources().getString(R.string.cadastroSalvo)+""+Integer.toString(i);
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

    public void Listar(View view) {
        startActivity(new Intent(this, ListaNotas.class));
    }

    public void Alterar(View view) {
        int id = Integer.parseInt(editCodigo.getText().toString());
        ContentValues cadastro = new ContentValues();
        cadastro.put("titulo",editTitulo.getText().toString());
        cadastro.put("texto",editTexto.getText().toString());
        int i = bd.update("notas",cadastro,"_id="+id,null);
        String s = getResources().getString(R.string.cadastros_afetados)+""+Integer.toString();
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

    public void Excluir(View view) {
        int id = Integer.parseInt(editCodigo.getText().toString());
        int i = bd.delete("notas","_id="+id, null);
        String s = getResources().getString(R.string.cadastroExcluido)+id;
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }
}